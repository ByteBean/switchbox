#SwitchBox

This application switches in front JW Library or Zoom

2024.06.20 / V1.5.3
Updated for compatibility with 'Zoom Workplace'

2022.04.08 / V 1.5
Major reworking of app to switch Zoom/JW Library instead of soundbox.

-- Moves Zoom window to same screen as JW Library & full screens
-- Enables easier switching between JW Library and Zoom during meetings