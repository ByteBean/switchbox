﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Craftedlogic.Core.HandleWindows;
using System.Windows.Forms;

namespace SwitchBox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {

        public MainWindow()
        {
            this.InitializeComponent();
            SetWindowPos();
        }

        private void jwLibraryButton_Click(object sender, RoutedEventArgs e)
        {
            this.ActivateJWLibraryTask();
        }
                
        public void ActivateJWLibraryTask()
        {
            Task task = new Task(() =>
            {
                ActivateJWLibraryInternal();
            });

            task.Start();
        }

        private void ActivateJWLibraryInternal()
        {
            try
            {
                var parentData = new FindWindows.SearchData() { WindowClass = null, Caption = "JW Library" };
                var childData = new FindWindows.SearchData() { WindowClass = "Windows.UI.Core.CoreWindow" };

                IWindowData windowRefData = WindowControl.Find(ref parentData, ref childData);
                
                //Thread.Sleep(10);
                WindowControl.Activate(windowRefData);
                var zoomParentData = new FindWindows.SearchData() { WindowClass = "ZPContentViewWndClass", Caption = "Zoom" };

                IWindowData zoomWindowData = WindowControl.Find(ref zoomParentData);
                //hide the zoom window!
                WindowControl.Activate(zoomWindowData, WindowsApi.ShowWindowEnum.Hide, false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
                
        private void altButton_Click(object sender, RoutedEventArgs e)
        { 
            Task task = new Task(() =>
            {
                ActivateAltWindow();
            });

            task.Start();
        }

        int loopCounter = 0;
        private void ActivateAltWindow()
        {
            try
            {
                var zoomParentData = new FindWindows.SearchData() { WindowClass = "ConfMultiTabContentWndClass", Caption = "Zoom Workplace" };

                IWindowData zoomWindowData = WindowControl.Find(ref zoomParentData);

                WindowControl.Activate(zoomWindowData, WindowsApi.ShowWindowEnum.ShowMaximized);
                WindowControl.ToFullScreen(zoomWindowData, WindowControl.GetScreenRightOfPrimary());
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private void SetWindowPos()
        {
            Properties.Settings settings = SwitchBox.Properties.Settings.Default;
            this.Top = settings.PosTop;
            this.Left = settings.PosLeft;
            this.Width = settings.WindowWidth;
            this.Height = settings.WindowHeight;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings settings = SwitchBox.Properties.Settings.Default;
            settings.PosTop = this.Top;
            settings.PosLeft = this.Left;
            settings.WindowHeight = this.Height;
            settings.WindowWidth = this.Width;
            settings.Save();
        }
    }
}
