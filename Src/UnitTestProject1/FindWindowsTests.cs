﻿using System;
using System.Diagnostics;
using Craftedlogic.Core.HandleWindows;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    /// <summary>
    /// FindWindows Unit Tests rely on FindWindows - use WindowControl instead .
    /// 
    /// </summary>
    [TestClass]
    public class FindWindowsTests
    {
        [TestMethod]
        public void Warning()
        {
            Assert.Inconclusive("These tests are not unit tests.\n\r  Only user functional testing is valid with this application.\n\r  FindWindows is incomplete and depreciated.\n\r  Use WindowControl Instead.");
        }

        [TestMethod]
        public void BringToFrontMetro()
        {
            var findWindows = new FindWindows();
            var data = new FindWindows.SearchData() {WindowClass = "Application", Caption = "JW Library"};
            findWindows.Search(ref data);
            Debug.Print("There are " + findWindows.Items.Count + " Windows Found");
                                                         
            foreach (var windowData in findWindows.Items)
            {
                Debug.Print(windowData.WindowHandle.ToString("X8"));
                Debug.Print(windowData.Caption);
                Debug.Print(windowData.ClassName);
            }
        }

        [TestMethod]
        public void IterateAllZoomWindows()                                           
        {
            var findWindows = new FindWindows();
            var data = new FindWindows.SearchData() { Caption = "Zoom"};
            findWindows.Search(ref data);
            Debug.Print("There are " + findWindows.Items.Count + " Windows Found");
            foreach (var windowData in findWindows.Items)
            {
                Debug.Print(windowData.WindowHandle.ToString("X8"));
                Debug.Print(windowData.Caption);
                Debug.Print(windowData.ClassName);
                Debug.Print("Status:"+windowData.Windowinfo.dwWindowStatus);
                Debug.Print("W:" + windowData.Windowinfo.rcWindow.Width);
                Debug.Print("Window IsDisabled:" + windowData.IsDisabled);
                Debug.Print("Window IsVisible:" + windowData.IsVisible);

                if (windowData.parentWindowData!=null) Debug.Print("Parent :"+windowData.parentWindowData.Caption);
            }
        }

        [TestMethod]
        public void BringToFrontJwLibrary()
        {
            //Strangely... in testing this finds and activates!... but when run on either a WPF / Winforms / Console exe... it does not!
            //Used above 'search' instead that picks up data via FindWindowsEx user32.dll call.

            //There are issues with EnumWindows that hides full screen 'universal/metro' apps

            //Find JW Lib process window and bring to front
            var windowSelector = new WindowSelector();
            windowSelector.FindAndActivateChild("ApplicationFrameInputSinkWindow", "JW Library");
         
            this.CountWindows();   
        }

        void CountWindows()
        {
            var enumerateChildrenWindows = new FindWindows();
            enumerateChildrenWindows.GetWindows();
            Debug.Print("There are " + enumerateChildrenWindows.Items.Count + " Windows Found");
        }

        [TestMethod]
        public void BringToFrontZoom()
        {
            //Find and bring to front
            var windowSelector = new WindowSelector();
            windowSelector.FindAndActivate("ConfMultiTabContentWndClass", "Zoom Workplace");
        }

        [TestMethod]
        public void ListProcesses()
        {
            //this is here for reference
            var  processes = Process.GetProcesses();

            foreach (var process in processes)
            {
                if (process.ProcessName.Contains("JW") || process.ProcessName.Contains("Sound"))
                {
                    Debug.Print(process.ProcessName);
                    Debug.Print(process.MainWindowTitle);
                }
            }
        }
    }
}
