﻿using System;

namespace Craftedlogic.Core.HandleWindows
{
    public interface IWindowData
    {
        IntPtr WindowHandle { get; set; }
        bool WindowFound { get; set; }
        string Caption { get; set; }
        string ClassName { get; set; }
        
        bool IsVisible { get; }

        bool IsDisabled { get; }

        WINDOWINFO Windowinfo { get; set; }

        IWindowData parentWindowData { get; set; }
    }
}