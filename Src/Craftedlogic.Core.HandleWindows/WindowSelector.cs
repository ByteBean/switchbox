﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Craftedlogic.Core.HandleWindows
{
    public class WindowSelector
    {
        public int CountWindowsEnum()
        {
            var enumerateChildrenWindows = new FindWindows();
            enumerateChildrenWindows.GetWindows();
            return enumerateChildrenWindows.Items.Count;
        }

        public void FindAndActivate( ref FindWindows.SearchData data)
        { 
            var findWindows = new FindWindows();
            findWindows.Search(ref data);
            Debug.Print("There are " + findWindows.Items.Count + " Windows Found");
            foreach (var windowData in findWindows.Items)
            {
                Debug.Print(windowData.WindowHandle.ToString("X8"));
                Debug.Print(windowData.Caption);
                Debug.Print(windowData.ClassName);
                //this is here to prevent accidents...  
                if (windowData.Caption != "JW Library") continue;

                IntPtr ptr = WindowsApi.FindWindowEx(windowData.WindowHandle, IntPtr.Zero,
                    "ApplicationFrameInputSinkWindow", null);

                if (ptr != IntPtr.Zero)
                    Debug.Print("JW Media Window Found ! :" + ptr.ToString("X8"));

                var jwWindow = findWindows.CreateWindowDataFromHandle(ptr);
                WindowsApi.ShowWindow(windowData.WindowHandle, WindowsApi.ShowWindowEnum.Restore);
                WindowsApi.SetForegroundWindow(windowData.WindowHandle);
                Thread.Sleep(10);
            }
        }

        
        public void FindAndActivate(string windowclass, string caption)
        {
            this._findWindows.GetWindows(windowclass, caption);

            foreach (var windowData in this._findWindows.Items)
            {
                this.ActivateWindow(windowData);
            }
        }

        public void FindAndActivateChild(string childClass, string mainWindowCaption)
        {
            this.FindAndActivateChild("", childClass, mainWindowCaption);
        }

        private readonly FindWindows _findWindows = new FindWindows();
        public FindWindows FindWindows { get { return _findWindows; } }

        public void FindAndActivateChild(string windowClass, string childClass, string mainWindowCaption)
        {
            this._findWindows.GetWindows(windowClass, mainWindowCaption);
            Debug.Print(this._findWindows.Items.Count + " Windows Found" );
            foreach (var windowData in this._findWindows.Items)
            {
                this.ActivateWindow(windowData);
                Debug.Print("Window >>>" + windowData.ClassName + ">>>" + windowData.Caption);
                this.FindActivateChildWindow(windowData, childClass);
            }
        }     


        public void FindActivateChildWindow(IWindowData parent, string windowClass)
        {
            var enumerateChildrenWindows = new FindWindows();
            var searchData = new FindWindows.SearchData() {WindowClass = windowClass};
            enumerateChildrenWindows.GetWindows(parent.WindowHandle, ref searchData);

            foreach (var item in enumerateChildrenWindows.Items)
            {
                Debug.Print("Found >>>" + item.ClassName+ ">>>" + item.Caption);
                this.ActivateWindow(item);
            }
        }


        public void ActivateWindow(IWindowData windowData)
        {
            //never attempt to activate a zero pointer window
            if (windowData.WindowHandle == IntPtr.Zero) return;

            Debug.Print("Activating " + windowData.Caption + ":" + windowData.WindowHandle.ToString("X8"));
            WindowsApi.ShowWindow(windowData.WindowHandle, WindowsApi.ShowWindowEnum.Restore);
            WindowsApi.SetForegroundWindow(windowData.WindowHandle);
        }
    }
}
