﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Craftedlogic.Core.HandleWindows
{
    public static class WindowControl
    {
        public static IWindowData Find(ref FindWindows.SearchData parentData, ref FindWindows.SearchData childData)
        {
            if (parentData == null && childData == null) return null;

            var parentPtr = IntPtr.Zero;
            var childPtr = IntPtr.Zero;
            
            IWindowData windowData = new WindowData();
            
            parentPtr = WindowsApi.FindWindow(parentData.WindowClass, parentData.Caption);

            if (parentPtr == IntPtr.Zero) return windowData;

            if (childData == null) return CreateWindowDataFromHandle(parentPtr); ;

            childPtr = WindowsApi.FindWindowEx(parentPtr, IntPtr.Zero, childData.WindowClass, childData.Caption);

            if (childPtr == IntPtr.Zero) return CreateWindowDataFromHandle(parentPtr);

            windowData = CreateWindowDataFromHandle(childPtr);
            windowData.parentWindowData = CreateWindowDataFromHandle(parentPtr);
            return windowData;
        }

        public static IWindowData Find(ref FindWindows.SearchData parentData)
        {
            return Find(ref parentData, ref FindWindows.SearchData.Null);
        }

        public static IWindowData CreateWindowDataFromHandle(IntPtr hWnd)
        {
            var windowData = new WindowData()
            {
                WindowHandle = hWnd
            };

            var sb = new StringBuilder(1024);
            WindowsApi.GetClassName(hWnd, sb, sb.Capacity);
            windowData.ClassName = sb.ToString();
            sb.Clear();
            WindowsApi.GetWindowText(hWnd, sb, sb.Capacity);
            windowData.Caption = sb.ToString();

            WINDOWINFO windowinfo = new WINDOWINFO();
            WindowsApi.GetWindowInfo(hWnd, ref windowinfo);
            windowData.Windowinfo = windowinfo;

            return windowData;
        }

        public static void Activate(IWindowData windowData)
        {
            Activate(windowData, WindowsApi.ShowWindowEnum.ShowNoActivate, true);
        }

        public static void Activate(IWindowData windowData, WindowsApi.ShowWindowEnum windowEnum)
        {
            Activate(windowData, windowEnum, true);
        }
        
        public static void Activate(IWindowData windowData, WindowsApi.ShowWindowEnum windowEnum, bool setForegroundWindow)
        {
            //never attempt to activate a zero pointer window
            if (windowData.WindowHandle == IntPtr.Zero) return;

            Debug.Print("Activating " + windowData.Caption + ":" + windowData.WindowHandle.ToString("X8"));
            WindowsApi.ShowWindow(windowData.WindowHandle, windowEnum);
            if (setForegroundWindow) WindowsApi.SetForegroundWindow(windowData.WindowHandle);

            if (windowData.parentWindowData == null) return;

            if (windowData.parentWindowData.WindowHandle != IntPtr.Zero)
                Activate(windowData.parentWindowData);
        }

        public static void updateWindowCoords(IWindowData windowData)
        {
            RECT rectangle = new RECT
            {
                
            };
           

            WindowsApi.DwmGetWindowAttribute(windowData.WindowHandle, DwmWindowAttribute.DWMWA_EXTENDED_FRAME_BOUNDS, out rectangle, 32);

            WINDOWINFO windowInfo = windowData.Windowinfo;
            windowInfo.rcWindow = rectangle;
        }

        public static void Position(IWindowData windowData)
        {

            WindowsApi.SetWindowPos(
                windowData.WindowHandle, IntPtr.Zero,
                windowData.Windowinfo.rcWindow.X,
                windowData.Windowinfo.rcWindow.Y,
                windowData.Windowinfo.rcWindow.Width,
                windowData.Windowinfo.rcWindow.Height,
                SetWindowPosFlags.ShowWindow | SetWindowPosFlags.FrameChanged & ~SetWindowPosFlags.DrawFrame);
         }

        public static void ToFullScreen(IWindowData windowData, Screen screen)
        {
            if (screen == null) screen = Screen.PrimaryScreen;

            WINDOWINFO windowInfo = windowData.Windowinfo;
            var screenBounds = screen.Bounds;

            screenBounds.Inflate((int)windowInfo.cxWindowBorders, (int)windowInfo.cyWindowBorders);

            windowInfo.rcWindow = new RECT(screenBounds);
            windowInfo.dwWindowStatus = 1;

            windowData.Windowinfo = windowInfo;
            

            //remote title
            //WindowStyles dwStyle = WindowsApi.GetWindowLongPtr(windowData.WindowHandle, WindowLongFlags.GWL_STYLE);
            WindowStyles dwStyle = windowData.Windowinfo.dwStyle;

            dwStyle &= ~WindowStyles.WS_CAPTION;
            WindowsApi.SetWindowLongPtr(windowData.WindowHandle, WindowLongFlags.GWL_STYLE, dwStyle);
            
            WindowControl.Position(windowData);
            WindowsApi.SetWindowRgn(windowData.WindowHandle, IntPtr.Zero, false);
        }

        /// <summary>
        /// Get Screen Rightmost of Primary
        /// </summary>
        /// <returns>Screen Rightmost of primary</returns>
        public static Screen GetScreenRightOfPrimary()
        {
            Screen screen = Screen.AllScreens.First();
            Screen primaryScreen = Screen.PrimaryScreen;

            foreach (var s in Screen.AllScreens)
            {
                if (s.WorkingArea.X == primaryScreen.WorkingArea.Width)
                {
                    screen = s;
                }
            }
            return screen;
        }
    }
}
