﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Craftedlogic.Core.HandleWindows
{
    public class FindWindows
    {
        public class SearchData
        {
            public string WindowClass;
            public string Caption;
            public IntPtr WindowHandle;
            public static SearchData Null;
        }

        public IWindowData CreateWindowDataFromHandle(IntPtr hWnd)
        {
            var windowData = new WindowData()
            {
                WindowHandle = hWnd
            };

            var sb = new StringBuilder(1024);
            WindowsApi.GetClassName(hWnd, sb, sb.Capacity);
            windowData.ClassName = sb.ToString();
            sb.Clear();
            WindowsApi.GetWindowText(hWnd, sb, sb.Capacity);
            windowData.Caption = sb.ToString();

            WINDOWINFO windowinfo = new WINDOWINFO();
            WindowsApi.GetWindowInfo(hWnd, ref windowinfo);
            windowData.Windowinfo = windowinfo;

            return windowData;
        }

        private bool KeepSearching(IntPtr hWnd, ref SearchData searchData)
        {
            var windowData = this.CreateWindowDataFromHandle(hWnd);
            
            if (searchData.WindowHandle == IntPtr.Zero && string.IsNullOrEmpty(searchData.Caption) && string.IsNullOrEmpty(searchData.WindowClass))
            {
                // no filter is applied, add item and return
                this.Items.Add(windowData);
            }
            else //only add if matched
            {
                if (windowData.WindowHandle == searchData.WindowHandle)
                {
                    this.Items.Add(windowData);
                    return true;
                }

                if (!string.IsNullOrEmpty(searchData.WindowClass) &&
                    windowData.ClassName.StartsWith(searchData.WindowClass) &&
                    string.IsNullOrEmpty(searchData.Caption))
                {
                    this.Items.Add(windowData);
                    return true;
                }

                if (!string.IsNullOrEmpty(searchData.Caption) &&
                        windowData.Caption.StartsWith(searchData.Caption) &&
                        string.IsNullOrEmpty(searchData.WindowClass)
                        )
                {
                    this.Items.Add(windowData);
                    return true;
                }
                
                if (!string.IsNullOrEmpty(searchData.WindowClass) && !string.IsNullOrEmpty(searchData.Caption) && 
                       windowData.ClassName.StartsWith(searchData.WindowClass) &&
                       windowData.Caption.StartsWith(searchData.Caption)
                       )
                {
                    this.Items.Add(windowData);
                         return true;
                }
            }

            return true;
        }
        
        public List<IWindowData> Items { get; private set; }

        /// <summary>
        /// This method searches for windows using API calls to FindWindowEx ... 
        /// the only function in windows that returns metro / universal windows that are full screen!
        /// </summary>
        /// <param name="searchData"></param>
        /// <param name="searchLimit"></param>
        public void Search(ref SearchData searchData, ulong searchLimit = 1000)
        {
            this.Items = new List<IWindowData>();

            IntPtr childWindow = IntPtr.Zero;
            ulong i = 0;
            
            while (i < searchLimit)
            {
                childWindow = WindowsApi.FindWindowEx(IntPtr.Zero, childWindow, null, null);
                if (!this.KeepSearching(childWindow, ref searchData))
                    break;

                i++;
            }
        }

        public void SearchByThreads(uint threadId, string wndclass, string title)
        {
            var searchData = new SearchData { WindowClass = wndclass, Caption = title };
            WindowsApi.EnumThreadWindows(threadId, this.KeepSearching,ref searchData);
        }

        internal void SearchByThreads(string processName, string windowclass, string caption)
        {
            this.Items = new List<IWindowData>();

            // Find process.
            Process[] p = Process.GetProcessesByName(processName);
            foreach (Process proc in p)
            {
                foreach (ProcessThread pt in proc.Threads)
                {
                    this.SearchByThreads((uint)pt.Id, windowclass, caption);
                }
            }
        }

        public void GetWindows(string wndclass, string title)
        {
            var searchData = new SearchData { WindowClass = wndclass, Caption = title };
            this.GetWindows(ref searchData);
        }

        public void GetWindows()
        {
            var searchData = new SearchData();
            this.GetWindows(ref searchData);
        }

        public void GetWindows(IntPtr hWndParent)
        {
            var searchData = new SearchData();
            this.GetWindows(hWndParent, ref searchData);
        }

        public void GetWindows(ref SearchData searchData)
        {
            this.Items = new List<IWindowData>();
            WindowsApi.EnumWindows(this.KeepSearching, ref searchData);
        }

        public void GetWindows(IntPtr hWndParent, ref SearchData searchData)
        {
            this.Items = new List<IWindowData>();
            WindowsApi.EnumChildWindows(hWndParent, this.KeepSearching, ref searchData);
        }
    }
}
