﻿using System;

namespace Craftedlogic.Core.HandleWindows
{
    internal class WindowData : IWindowData
    {
        public IntPtr WindowHandle { get; set; }
        public bool WindowFound { get; set; }
        public string Caption { get; set; }
        public string ClassName { get; set; }

        public bool IsVisible => (this.Windowinfo.dwStyle & WindowStyles.WS_VISIBLE) != 0;

        public bool IsDisabled => (this.Windowinfo.dwStyle & WindowStyles.WS_DISABLED) != 0;

        public WINDOWINFO Windowinfo { get; set; }
        public IWindowData parentWindowData { get; set; }
    }
}