#SwitchBox
Written by Nicholas Bean, Okehampton, Devon, UK
This utility is for easier in meeting switching video output from JW Library or Zoom.US
For features and updates see [here](https://bitbucket.org/ByteBean/switchbox/wiki/Home)

bytebean at craftedlogic dot net

This software is not to be charged for or sold for profit but is licensed
Copyright (c) 2022 Nicholas Bean

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to use 
the Software, copy, publish, distribute and to permit persons to whom the Software is
furnished to do so but not to sublicense, and/or profit from selling copies of the Software, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

bytebean at craftedlogic dot net

All trademarks are the property of their respective owners.