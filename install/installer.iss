; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "Switchbox"
#define MyAppVersion "1.5.7 Zoom Workplace"
#define MyAppPublisher "Nik Bean [Bytebean]"
#define MyAppURL "https://bitbucket.org/ByteBean/switchbox/wiki/Home"
#define MyAppExeName "SwitchBox.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{B36A274F-FA72-4D5F-8704-166829582899}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
AppCopyright={#MyAppURL}
DefaultDirName={autopf}\{#MyAppName}
DisableProgramGroupPage=yes
LicenseFile=C:\_\ByteBean\Switcher\LICENSE.txt
; Uncomment the following line to run in non administrative install mode (install for current user only.)
;PrivilegesRequired=lowest
OutputDir=C:\_\ByteBean\Switcher\install
OutputBaseFilename=SwitchBoxZWPInstaller
SetupIconFile=C:\_\ByteBean\Switcher\media\SwitchBoxIcon.ico
;Compression=none
SolidCompression=yes
WizardStyle=modern


[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\_\ByteBean\Switcher\Src\Presentation\bin\Release\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\_\ByteBean\Switcher\Src\Presentation\bin\Release\Craftedlogic.Core.HandleWindows.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\_\ByteBean\Switcher\Src\Presentation\bin\Release\SwitchBox.exe.config"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: waituntilterminated postinstall skipifsilent runasoriginaluser

